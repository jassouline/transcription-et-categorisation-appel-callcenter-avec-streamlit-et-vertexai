import streamlit as st
from google.cloud import storage
from google.cloud.speech_v2 import SpeechClient
from google.cloud.speech_v2.types import cloud_speech
import vertexai
from vertexai.language_models import TextGenerationModel

# Configuration initiale
BUCKET_NAME = 'audio-transcription-demo-genai'
PROJECT_ID = "genai-use-cases"
LOCATION = "us-central1"
MODEL = "text-bison@002"


def upload_to_gcs(bucket_name, file):
    """Fonction pour uploader un fichier dans un bucket GCS."""
    try:
        storage_client = storage.Client()
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(file.name)
        blob.upload_from_file(file, content_type='audio/mp3')

        gcs_uri = f"gs://{bucket_name}/{file.name}"
        st.success(f"Le fichier '{file.name}' a été téléchargé avec succès dans le bucket GCS '{bucket_name}'.")

        return gcs_uri
    except Exception as e:
        st.error(f"Une erreur s'est produite lors de l'upload du fichier : {e}")
        return None


def transcribe_batch_gcs_input_inline_output_v2(
    project_id: str,
    gcs_uri: str,
) -> cloud_speech.BatchRecognizeResults:
    """ Transcription d'un audio depuis une URI Google Cloud Storage

    Args:
        project_id: L'ID du projet Google Cloud
        gcs_uri: L'URI de Google Cloud Storage à utiliser

    Returns:
        Le RecognizeResponse.
    """
    # Instanciation du client
    client = SpeechClient()

    config = cloud_speech.RecognitionConfig(
        auto_decoding_config=cloud_speech.AutoDetectDecodingConfig(),
        language_codes=["fr-FR"],
        model="long",
    )

    file_metadata = cloud_speech.BatchRecognizeFileMetadata(uri=gcs_uri)

    request = cloud_speech.BatchRecognizeRequest(
        recognizer=f"projects/{project_id}/locations/global/recognizers/_",
        config=config,
        files=[file_metadata],
        recognition_output_config=cloud_speech.RecognitionOutputConfig(
            inline_response_config=cloud_speech.InlineOutputConfig(),
        ),
    )

    # Transcription de l'audio en texte
    operation = client.batch_recognize(request=request)
    st.write("En attente de la transcription...")
    response = operation.result(timeout=240)

    transcriptions = []
    for result in response.results[gcs_uri].transcript.results:
        transcriptions.append(result.alternatives[0].transcript)

    transcription_final = " ".join(transcriptions)
    return transcription_final


def summarise_patient_transcription(audio_transcription, PROJECT_ID=PROJECT_ID):
    vertexai.init(project=PROJECT_ID, location=LOCATION)
    parameters = {
        "max_output_tokens": 1024,
        "temperature": 0.7,
        "top_p": 0.8,
        "top_k": 40
    }
    model = TextGenerationModel.from_pretrained(MODEL)

    response = model.predict(
        """Tu appartiens à la société \"Assouline\" qui s\'occupe de la livraison de repas à domicile.
Tu vas obtenir la transcription d\'une conversation entre un employé de notre centre d\'appel et un client.
Premièrement, tu vas identifier et séparer les parties de la conversation qui correspondent au client, et celles qui correspondent à l\'employé du centre d\'appel. Tu afficheras le mot <operateur> avant chaque phrase correspondante à l\'opérateur et le mot <client> avant chaque phrase correspondante au client.
Deuxièmement, tu vas mettre une phrase courte de résumé de la conversation.
Troisièmement, tu vas afficher la transcription de la manière suivante en identifiant et indiquant les différents interlocuteurs :
            <operateur> Bonjour je m\'appelle Emilie\n
            <client> Bonjour Emilie, je suis Marc et je ne suis pas content\n
            <operateur> Comment puis-je vous aider ?\n
Quatrièmement, tu vas afficher le sentiment du client. Parmi les choix possibles tu auras : très négatif, négatif, neutre, positif, très positif.
Cinquièmement, tu vas définir la catégorie à laquelle appartient l\'appel (maximum 3 catégories) parmi les suivants : retard, mauvaise qualité, remboursement, réduction, harcèlement, erreur de commande, endommagé, mauvais livreur. Tu afficheras le résultat sous la forme : \"Tag : remboursement, retard\" par exemple.

call_transcription: \"bonjour vous êtes en ligne avec la société Assouline je m\'appelle Émilie comment puis-je vous aider aujourd\'hui bonjour je suis extrêmement mécontent mon repas est arrivé froid est en retard c\'est inacceptable\"
output: \"Résumé : Appel auprès d\'Emilie pour un repas arrivé froid et en retard\n
<operateur> - Bonjour vous êtes en ligne avec la société Assouline. Je m\'appelle Émilie, comment puis-je vous aider aujourd\'hui ?\n
<client> - Bonjour, je suis extrêmement mécontent. Mon repas est arrivé froid et en retard, c\'est inacceptable\n\"
\n
Sentiment : Très négatif\n
Tag : retard, mauvaise qualité\n\"

call_transcription: {}
output:
""".format(audio_transcription), **parameters)

    return (response.text)


def main():
    st.title("Upload de fichier mp3 d'un appel au call center, transcription et catégorisation")

    uploaded_file = st.file_uploader("Choisissez un fichier WAV", type=['mp3'])
    if uploaded_file is not None:
        gcs_uri = upload_to_gcs(BUCKET_NAME, uploaded_file)
        if gcs_uri:
            transcription = transcribe_batch_gcs_input_inline_output_v2(project_id=PROJECT_ID,
                                                                        gcs_uri=gcs_uri)
            st.write("Transcription:")
            st.text_area("", transcription, height=200)
            # Bouton GenAI pour générer le texte
            if st.button('GenAI pour formater'):
                # Assurez-vous que transcription_area n'est pas vide
                if transcription:
                    generated_text = summarise_patient_transcription(audio_transcription=transcription,
                                                                     PROJECT_ID=PROJECT_ID)
                    st.write("Texte Généré:")
                    st.markdown(generated_text, unsafe_allow_html=True)
                else:
                    st.error("La transcription est vide. Veuillez d'abord uploader et transcrire un fichier audio.")


if __name__ == '__main__':
    main()
